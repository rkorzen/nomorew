import os
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class BaseFileSystemStorage(FileSystemStorage):
    def __init__(self, location=None, base_url=None, **kwargs):
        location = location or settings.MEDIA_ROOT
        base_url = base_url or settings.MEDIA_URL

        if not os.path.exists(location):
            os.makedirs(location)

        super(BaseFileSystemStorage, self).__init__(
            location, base_url, **kwargs)

    def name_from_url(self, url):
        if url:
            name = url.replace(self.base_url, '')
            if self.exists(name):
                return name
        return None


class RefinementsStorage(BaseFileSystemStorage):
    def __init__(self, location=None, base_url=None, **kwargs):
        location = location or settings.REFINEMENTS_MEDIA_ROOT
        base_url = base_url or '%s' % settings.REFINEMENTS_MEDIA_URL
        super().__init__(location=location, base_url=base_url, **kwargs)

class ResultsStorage(BaseFileSystemStorage):
    def __init__(self, location=None, base_url=None, **kwargs):
        location = location or settings.RESULTS_MEDIA_ROOT
        base_url = base_url or '%s' % settings.RESULTS_MEDIA_URL
        super().__init__(location=location, base_url=base_url, **kwargs)


AVAILABLE_STORAGES = {
    'refinements': RefinementsStorage,
    'results': ResultsStorage,
}


def get_storage(storage_name, location=None, base_url=None):
    storage_name = storage_name or 'images'
    try:
        cls_storage = AVAILABLE_STORAGES[storage_name]
    except KeyError:
        cls_storage = BaseFileSystemStorage
    return cls_storage(location=location, base_url=base_url)
