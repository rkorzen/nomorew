# NOMORE README :D

## Install

Clone repository

    git clone https://github.com/rkorzen/nomorew.git

Create virtualenv

    python3 -m venv venv

Activate env

    source venv/bin/activate

Install requirements

    pip install -r requirements.txt

Create .env file (in the same folder as settings.py) and add parameters to .env file at least:

    SECRET_KEY=
    EMAIL_HOST=
    EMAIL_HOST_USER=
    EMAIL_HOST_PASSWORD=
    EMAIL_PORT=
    EMAIL=

Migre

    python manage.py migrate

## RUN

###  redis

Make sure that redis is working on port 6379


### celery

This command can be call with use of screner

    screen -ls
    screen -r celery (if there is such screen) 
or

    screen -S celery

and then

    celery -A nomorew worker -l info

    ctrl + a ctrl + d - to detach screen

### application

Similar as celery - but in gunicorn screen

    gunicorn -b 0.0.0.0:80 nomorew.wsgi



