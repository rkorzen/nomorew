import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nomorew.settings')

app = Celery('nomorew')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


# app.conf.update(
#     task_serializer='pickle',
#     result_serializer='pickle'
# )