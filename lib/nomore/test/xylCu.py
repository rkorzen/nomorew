#!/usr/bin/env python

import os
import sys

sys.path.append("..")
from nomore import NoMoRe

CrystalOutputFile = 'xylCu.out'  # Crystal Frequency output
ShelxInputFile = 'xylitol_Cu.ins'       # Template input to shelxl
ShelxHKLFile = 'xylitol_Cu.hkl'
T = 122                              # The temperature.
eliminate = -3                     # The number of frequencies to eliminate.
# correction of anharmonicity for X-H stretch vibrations.
anharm_corr = 174.
REFINE = '0-17'                    # modes to refine


NoMoRe.refine(CrystalOutputFile,ShelxInputFile,ShelxHKLFile,T=T,eliminate=eliminate,REFINE=REFINE)
#NoMoRe.refine(CrystalOutputFile, ShelxInputFile)
