"""
Python program for normal-mode refinement.

copyleft,
Anders O. Madsen
Anna A. Hoser
"""

import os
import re
import sys
import zipfile
import numpy as np
from scipy import optimize
import pickle
import shutil
import subprocess
import logging
from .NoLib.reader import readed_f
from .NoLib.creating_shelx_ins import create_shelx, final_shelx_coordinates
from .NoLib.interpret import interpret, find_symmetry, expand
from .NoLib.write_cif import write_final_cif, pickle_ADPs
from django.conf import settings
import shutil


def shelxl(parameters, degeneracy, refine, T, normalmodes_orig, frequencies, atoms, symmatomcoord, cell, eliminate,
           ShelxInputFile, files):
    sg = expand(parameters, degeneracy, refine, atoms)
    return np.array(
        create_shelx(degeneracy, refine, T, sg, normalmodes_orig, frequencies, atoms, symmatomcoord, cell, eliminate,
                     ShelxInputFile, files)[0])


def set_working_directory():
    base = settings.SHELXL_CALCULATIONS_BASE
    base.mkdir(exist_ok=True, parents=True)
    i = 0
    new_dir_temp = "calc_{}"
    while new_dir_temp.format(i) in os.listdir(base):
        i += 1
    new_dir = os.path.join(base, new_dir_temp.format(i))
    os.mkdir(new_dir)
    os.chdir(new_dir)
    shutil.copy(settings.SHELXL_PATH, ".")


def set_zip_file_name():
    zip_file_name_tmp = "results_{}.zip"
    files = os.listdir()
    i = 1
    while zip_file_name_tmp.format(i) in files:
        i += 1
    return zip_file_name_tmp.format(i)


def refine(
        CrystalOutputFile,
        ShelxInputFile,
        ShelxHKLFile,
        T=123,
        eliminate=-3,
        anharm_corr=174,
        REFINE='0-6',
        starting_parameters=None
):
    old_cwd = os.getcwd()
    print("Before: ", old_cwd)
    set_working_directory()
    print("After: ", os.getcwd())

    shelxl_path = "./shelxl"

    zip_file_name = set_zip_file_name()

    # logging.basicConfig(filename='NoMoRe.log', level=logging.DEBUG)

    # open output files
    try:
        with open('NoMoRe.log', 'w') as logfile, open('NoMoRe.debug', 'w') as debugfile, open('NoMoRe.frequencies',
                                                                                              'w') as datafile:

            # logfile = open('NoMoRe.log', 'w')
            # debugfile = open('NoMoRe.debug', 'w')
            # datafile = open('NoMoRe.frequencies', 'w')
            files = (logfile, debugfile, datafile)

            refine = interpret(REFINE)

            normalmodes_orig, cell, freqnum, frequencies, atoms, symmatomcoord = readed_f(CrystalOutputFile, files)

            logfile.write('Welcome to the NoMore output file! \n')
            logfile.write(f'Temperature {T} K \n')

            parameters = [1.0] * len(refine)
            degeneracy = find_symmetry(frequencies)

            if starting_parameters:
                parameters = starting_parameters + parameters[len(starting_parameters):]

            debugfile.write('Degeneracy: \n')
            debugfile.writelines(["%s " % item for item in degeneracy])
            debugfile.write('\n')

            debugfile.write('Refined frequencies \n')
            debugfile.writelines(["%s " % item for item in refine])
            debugfile.write('\n')

            logfile.write('Number of refined frequencies %i' % len(refine))
            logfile.write('\n')

            ref = open('numb_ref_freq.data', 'wb')
            pickle.dump(refine, ref)
            ref.close()

            # make sure a test.hkl file is available
            shutil.copyfile(ShelxHKLFile, 'test.hkl')

            # make a first run on the test files, to generate a test.fcf
            logfile.write('Running shelxl \n')
            p = subprocess.Popen([shelxl_path, 'test'], stdout=subprocess.PIPE)
            p.wait()
            if p.returncode == 0:
                logfile.write('first run of shelxl was successful. \n')
            if p.returncode == 1:
                logfile.write('first run of shelxl was not successful. \n')

            debugfile.write('Progress of refinement - wR2: \n')
            low = np.zeros_like(parameters)
            high = np.ones_like(parameters) * 6.0
            bounds = (low, high)

            # import numdifftools as nd

            if REFINE == 'None':
                logfile.write('No refinement requested, stopping! \n')
                sys.exit()

            result = optimize.least_squares(
                shelxl, parameters,
                bounds=bounds,
                jac='3-point',
                diff_step=0.007,
                loss='linear',
                gtol=0.000001,
                ftol=0.000001,
                xtol=0.0000001,
                args=(
                    degeneracy, refine, T, normalmodes_orig,
                    frequencies, atoms, symmatomcoord, cell,
                    eliminate, ShelxInputFile, files
                ),
                verbose=2)

            with open('least_squares_result.data', 'wb') as ref:
                pickle.dump(result, ref)

            # collect further information to dump in a pickled file.
            # making a last run of shelxl
            parameters = result['x']

            logfile.write('Final shelxl run with optimized parameters \n')
            shelxl(parameters, degeneracy, refine, T, normalmodes_orig, frequencies, atoms, symmatomcoord, cell,
                   eliminate,
                   ShelxInputFile, files)

            with open("test.res") as res:
                f1 = res.readlines()

            with open("test.lst") as lst:
                f2 = lst.readlines()

            with open("test.fcf", "r") as fcf:
                f3 = fcf.readlines()
            all = (f1, f2, f3)

            with open('shelxl_results.p', 'wb') as pickle_file:
                pickle.dump(all, pickle_file)

            debugfile.write('RESULT of least squares \n')
            for item in result:
                debugfile.write("%s %s \n" % (item, result[item]))

            logfile.write('Making final run of shelxl to produce cif. \n')

            # tu powstaje test.cif
            final_shelx_coordinates(files)
            sg = expand(parameters, degeneracy, refine, atoms)
            pickle_ADPs(degeneracy, refine, T, sg, normalmodes_orig, frequencies, atoms, symmatomcoord, cell, eliminate,
                        files)

            # final frequencies to logfile and to separate data file
            logfile.write('The final frequencies\n')
            for i in range(len(sg)):
                modified_frequencies = [50.0, 50.0, 50.0] + frequencies[3:]
                f = modified_frequencies[i] * sg[i]
                logfile.write("%6.1f   " % f)
                datafile.write("%6.3f   \n " % f)
                if i % 5 == 4:
                    logfile.write('\n')

            # close files
            # logfile.close()
            # debugfile.close()
            # datafile.close()

            return zip_file_name
    except Exception as e:
        raise e
