from math import exp
from scipy import dot,transpose,array, eye, linalg
from .Crystallographic import ADPcart2frac
import copy

"""
Python module to calculate msds and next ADPs in given temperature.

copyleft,
Anders O. Madsen
Feb. 2008.

"""

ha_cm1=219474.631371
amu_me=1822.88848
cm1_ha=1./ha_cm1 



def msd_calc(degeneracy,refine,T, sg, normalmodes_orig, frequencies_orig,atoms,symmatomcoord,cell, eliminate,files):

        (logfile,debugfile,datafile) = files


        freqnum = len(frequencies_orig)
        frequencies = frequencies_orig[:]
        normalmodes = copy.copy(normalmodes_orig)
        

        if eliminate > 0:
                        for l in range(eliminate):
                                frequencies[l] = 5E4
        if eliminate < 0:
                        for l in range(abs(eliminate)):
                                frequencies[l] = 50.0 
        
        

        def msd(freq,T):
                if freq >= 5000:
                        freq = 5000.
                u2 = 33.69/freq*(0.50+1/(exp(freq/(0.695*T))-1))
                return u2


        for j in range(len(frequencies)):
            frequencies[j] = frequencies[j]*sg[j]


        # the lowest frequency eigenmodes are set to zero!
        if eliminate > 0:
                for i in range(eliminate):
                        normalmodes[i] = [0]*freqnum
        if eliminate < 0:
                pass


        debugfile.writelines(["%s " % item  for item in frequencies])
        debugfile.write('\n')
        # form diagonal matrix of mean square displacements.

        delta = eye(atoms*3) # unit matrix of dimensions:  atoms*3,atoms*3
        for f in range(freqnum):
                
            delta[f,f] = msd(frequencies[f],T)
                
        # form the product to get the matrix of MSDs

        msdmatrix = dot(transpose(normalmodes),dot(delta,normalmodes))
        
        ADPs = []
        ADPst = []
        ADPsch = []
        counter = 0
        string3 = ''
        string5 = ''
        c=[]
        k=''
        for a in range(0,freqnum,3):
                if symmatomcoord[counter][5] == 'T' or  symmatomcoord[counter][8] == 'T':
                    
                    A = msdmatrix[a:a+3,a:a+3]
                    E =  linalg.eig(A)
                    for i in range(3):
                            string = '%8.4f    %s'%(E[0][i].real,E[1][i].real)
                    string2 = '%8.0f %8.0f %8.0f \n'%(E[0][0].real*1E4,E[0][1].real*1E4,E[0][2].real*1E4)


                    B = ADPcart2frac(cell,A)

                    Ba = array(B)
                    string3 = '%8.5f %8.5f =\n %8.5f %8.5f  %8.5f %8.5f ' \
                                             %(Ba[0][0],Ba[1][1],Ba[2][2],Ba[1][2],Ba[0][2],Ba[0][1])
                     
                    ADPs.append(string3)
                    string5 = string3.replace("=\n"," ")
                    UIJ=string5.split()
                    Bk = []
                    for i in range(6):  
                        c=float(UIJ[i])
                        if c>0.0:
                                d=10.0+c
                                k=d
                        else:   
                                a=10.0-abs(c)
                                k=a
                        Bk.append(k)
                    string4 = '%8.5f %8.5f =\n %8.5f %8.5f  %8.5f %8.5f '%(Bk[0],Bk[1],Bk[2],Bk[3],Bk[4],Bk[5])

                    ADPsch.append(string4)
                    if not symmatomcoord[counter][11] == 'H':
                        ADPst.append(string5)
                counter = counter + 1
        return msdmatrix, ADPs, ADPsch, ADPst

        



