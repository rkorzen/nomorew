from .msd_calculator import msd_calc
import pickle
import re


def pickle_ADPs(degeneracy, refine, T, sg, normalmodes_orig, frequencies, atoms, symmatomcoord, cell, eliminate, files):
    msdmatrix, ADPs, ADPsch, ADPst = msd_calc(
        degeneracy,
        refine,
        T,
        sg,
        normalmodes_orig,
        frequencies,
        atoms,
        symmatomcoord,
        cell,
        eliminate,
        files
    )
    with open('adps.data', 'wb') as f:
        pickle.dump(ADPs, f)
    # return ADPs


def write_final_cif():
    with open('adps.data', 'rb') as a:
        ADPS = pickle.load(a)

    number_of_atoms = len(ADPS)
    with open('test.cif', 'r') as incif:
        cifdata = incif.readlines()

    print("Cif data", cifdata)
    with open('final.cif', 'w') as outcif:
        pattern = re.compile(" _atom_site_aniso_U_12\n")
        for i in range(len(cifdata)):
            if pattern.search(cifdata[i]):
                for j in range(number_of_atoms):
                    cifdata[i + j + 1] = cifdata[i + j + 1].split()[0] + ADPS[j].replace('=\n', '') + '\n'
            outcif.write(cifdata[i])
