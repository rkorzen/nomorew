#! /usr/bin/python

"""
A program to compute the index to compare ADPs (Whitten and Spackman 06).

takes two lists of Uijs along with the cell parameters and
performs an orthogonalization, followed by the computation of

100 * (1 - 2^(2/3.)*(det(U1^(-1)*U2^(-1)))^(1/4.) / (det(U1^(-1) + U2^(-1)))^(1/2.))

Where U1 and U2 are the orthogonalized mean square displacement matrices.

What happens if we make the matrices unitary? Would the similarity index
then only tell us about the deviations in directions instead of a mixture
of msd size and direction?
"""

from .Crystallographic import ADPfrac2cart
from scipy.linalg import eig,inv,det
from numpy import *


def overlap(U1,U2,cell):
    #change to floating point
    U1f = []
    U2f = []
    for u in U1:
        uf = float(u)
        U1f.append(uf)
    for u in U2:
        uf = float(u)
        U2f.append(uf)

    #matrices:
    U1m = array([[U1f[0],U1f[3],U1f[4]],[U1f[3],U1f[1],U1f[5]],[U1f[4],U1f[5],U1f[2]]])
    U2m = array([[U2f[0],U2f[3],U2f[4]],[U2f[3],U2f[1],U2f[5]],[U2f[4],U2f[5],U2f[2]]])

    #orthogonalize:
    U1c = ADPfrac2cart(cell,U1m)
    U2c = ADPfrac2cart(cell,U2m)

    #invert:
    U1ci = inv(U1c)
    U2ci = inv(U2c)
    
    #compute the overlap index:
    overlapindex = 100*(1 - 2**(3/2.)*(det(dot(U1ci,U2ci)))**(1/4.) / (det(U1ci + U2ci))**(1/2.))

    return overlapindex


#U1 = (0.00470,  0.00396,  0.00768, -0.00040,  0.00000, -0.00000)
U1 = array([0.0321, 0.0266, 0.0249, -0.0117, -0.0033, 0.0081])
U2 = array([0.0321, 0.0266, 0.0249, -0.0117, -0.0033, 0.0081])

#cell = (1,1,1, 90., 90., 90.)
#cell = (8.262, 8.900, 8.923, 90.00, 90.00, 90.00)

#print overlap(U1,U2,cell)


