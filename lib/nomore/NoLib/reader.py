import re
from scipy import dot, transpose, matrix

"""
Python module to read CRYSTAL09 frequency output (cell, frequencies and normal modes)

copyleft,
Anders O. Madsen
Feb. 2008.
"""

#
# Here are new frequencies for some of the N-H stretching modes
# this is an anharmonicity correction (data from Mimmo).
# We have to subtract the following numbers:
# 6-31G(d,p) = 174 cm-1
# 6-311G(d,p) = 167 cm-1
# DZP=176 cm-1
# TZP=164 cm-1 
anharm_corr = 174.


def readed_f(inputfile, files):
    (logfile, debugfile, datafile) = files

    input = open(inputfile, 'r')

    data = input.readlines()

    a1 = re.compile('ATOMS IN THE UNIT CELL')
    a2 = re.compile('NUMBER OF SYMMETRY OPERATORS         : ')
    a3 = re.compile("LATTICE PARAMETERS  \(ANGSTROMS AND DEGREES\) - CONVENTIONAL CELL")
    a4 = re.compile("X\/A")

    for i in range(len(data)):
        am1 = a1.search(data[i])
        am2 = a2.search(data[i])
        am3 = a3.search(data[i])
        if am1:
            atoms = int(float(data[i][61:65]))
        if am2:
            symmop = int(float(data[i][41:44]))
        if am3:
            A = float(data[i + 2][0:12])
            B = float(data[i + 2][12:24])
            C = float(data[i + 2][24:36])
            Alpha = float(data[i + 2][36:48])
            Beta = float(data[i + 2][48:60])
            Gamma = float(data[i + 2][60:72])
            cell = (A, B, C, Alpha, Beta, Gamma)
            symmatomcoord = []
    for i in range(len(data)):
        am4 = a4.search(data[i])
        if am4:
            for j in range(int(atoms)):
                symmatomcoord.append(data[i + 2 + j])


    # number of frequencies
    freqnum = atoms * 3

    # conversion factors from Mimmo - to convert
    # 'normalized to classical amplitudes' --> 'cartesian coordinates'
    # frequencies must be converted to hartree/bohr**2/amu
    # and then used to un-normalize the modes
    ha_cm1 = 219474.631371
    amu_me = 1822.88848
    cm1_ha = 1. / ha_cm1

    # search for the string  NORMAL MODES NORMALIZED TO CLASSICAL AMPLITUDES
    p1 = re.compile('NORMAL MODES NORMALIZED TO CLASSICAL AMPLITUDES')
    for i in range(len(data)):
        m1 = p1.search(data[i])
        if m1:
            start = i
            ## harvest the frequencies ##
            frequencies = []
            # we have to harvest frequencies freqnum/6 times (because 6 freqs on each line)
            debugfile.write('freqnum')
            debugfile.write(f"{freqnum}")
            for j in range(int(freqnum / 6)):
                freqlinenumber = start + j * atoms * 3 + 2 + j * 3
                f = data[freqlinenumber].split()
                for i in range(1, 7):
                    frequencies.append(float(f[i]))

    # we form the matrix of normal modes
    # the xyz of each atom in a mode is a column.

    normalmodes = []

    for j in range(int(freqnum / 6)):
        v = [[], [], [], [], [], []]
        freqlinenumber = start + j * atoms * 3 + 2 + j * 3
        for k in range(2, atoms * 3 + 2):
            # the six normal modes on each line
            for l in range(6):
                v[l].append(float(data[freqlinenumber + k][14 + 10 * l:24 + 10 * l]))
        for l in range(6):
            normalmodes.append(v[l])

    normalmodes = matrix(normalmodes)

    for j in range(len(frequencies)):
        fact = (frequencies[j] * amu_me * cm1_ha) ** 2
        fact = abs(fact) ** 0.25
        normalmodes[j] = normalmodes[j] * fact

    for i in range(20):
        e = normalmodes[i]
        norm = dot(e, transpose(e))

    if frequencies[4] < 0:
        logfile.write(f"{frequencies[4]} At least four negative frequencies - please check calculations results ! \n")
    else:
        logfile.write("CRYSTAL output reading successfully finished :)\n")
    return normalmodes, cell, freqnum, frequencies, atoms, symmatomcoord
