#! /usr/bin/python
"""
Changes made on feb. 1. 2008:
Using the linalg module from scipy instead
of the LinearAlgebra module. this may
break things, but we'll see...
Also changing to the numpy module instead of
Numeric. Numpy is developed along with scipy.
"""

# from math import *
# from numpy import *
import numpy as np

"""
Takes a free format cartesian coordinate-file with the
first line as:

a b c alpha beta gamma

-- and the next as:

atom x y z


Anders Madsen, 27. february 2001.

"""


def cart2frac(cell, input):
    (a, b, c, alphadeg, betadeg, gammadeg) = cell
    (xcart, ycart, zcart) = input
    alpha = alphadeg / 360 * 2 * np.pi
    beta = betadeg / 360 * 2 * np.pi
    gamma = gammadeg / 360 * 2 * np.pi
    V = a * b * c * np.sqrt(1 - (np.cos(alpha)) ** 2 - (np.cos(beta)) ** 2 - (np.cos(gamma)) ** 2 \
                            + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    astar = b * c * np.sin(alpha) / V
    bstar = a * c * np.sin(beta) / V
    cstar = a * b * np.sin(gamma) / V
    # A is the conversion matrix
    A11 = a
    A21 = 0
    A31 = 0
    A12 = b * np.cos(gamma)
    A22 = b * np.sin(gamma)
    A32 = 0
    A13 = c * np.cos(beta)
    A23 = c * (np.cos(alpha) - np.cos(beta) * np.cos(gamma)) / np.sin(gamma)
    A33 = 1 / cstar

    zfrac = 1 / A33 * zcart
    yfrac = (ycart - A23 * zfrac) / A22
    xfrac = (xcart - A12 * yfrac - A13 * zfrac) / A11
    return [xfrac, yfrac, zfrac]


######FRAC2CART.PY:

"""
Takes a free format fractional coordinate-file with the
first line as:

a b c alpha beta gamma

-- and the next as:

atom x y z

Anders Madsen, 27. february 2001.

"""


def frac2cart(cell, frac):
    [a, b, c, alphadeg, betadeg, gammadeg] = cell
    [xfrac, yfrac, zfrac] = frac
    alpha = alphadeg / 360 * 2 * np.pi
    beta = betadeg / 360 * 2 * np.pi
    gamma = gammadeg / 360 * 2 * np.pi
    V = a * b * c * np.sqrt(1 - (np.cos(alpha)) ** 2 - (np.cos(beta)) ** 2 - (np.cos(gamma)) ** 2 \
                            + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    astar = b * c * np.sin(alpha) / V
    bstar = a * c * np.sin(beta) / V
    cstar = a * b * np.sin(gamma) / V

    # A is the tranformation matrix Xcart = A*Xfrac

    A11 = a
    A21 = 0
    A31 = 0
    A12 = b * np.cos(gamma)
    A22 = b * np.sin(gamma)
    A32 = 0
    A13 = c * np.cos(beta)
    A23 = c * (np.cos(alpha) - np.cos(beta) * np.cos(gamma)) / np.sin(gamma)
    A33 = 1 / cstar

    xcart = xfrac * A11 + yfrac * A12 + zfrac * A13
    ycart = xfrac * A21 + yfrac * A22 + zfrac * A23
    zcart = xfrac * A31 + yfrac * A32 + zfrac * A33
    return [xcart, ycart, zcart]


##### LENGTH OF CARTESIAN VECTOR ####

def length(vec):
    l = np.sqrt(vec[0] ** 2 + vec[1] ** 2 + vec[2] ** 2)
    return l


##### <u^2> calculation from frequency #####

# based on expression in Coppens

def msqdisp(freq, T, redmass):
    u2 = 33.69 / (redmass * freq) * (0.50 + 1 / (np.exp(freq / (0.695 * T)) - 1))
    return u2


##### Calculation of diatomic reduced mass #####


def redmass(atom1, atom2):
    m = {'C': 12.0107,
         'O': 15.9994,
         'N': 14.00674,
         'H': 1.00794,
         'D': 2.014101779,
         'F': 18.9984}

    mass = m[atom1] * m[atom2] / (m[atom2] + m[atom1])
    return mass


def cross(a, b):
    cross = [a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2], a[0] * b[1] - a[1] * b[0]]
    return cross


#####  Transformation from cartesian ADP's to crystal system


def ADPcart2frac(cell, cartADP):  # cartADP
    (a, b, c, alphadeg, betadeg, gammadeg) = cell
    alpha = alphadeg / 360 * 2 * np.pi
    beta = betadeg / 360 * 2 * np.pi
    gamma = gammadeg / 360 * 2 * np.pi
    V = a * b * c * np.sqrt(1 - (np.cos(alpha)) ** 2 - (np.cos(beta)) ** 2 - (np.cos(gamma)) ** 2 \
                            + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    astar = b * c * np.sin(alpha) / V
    bstar = a * c * np.sin(beta) / V
    cstar = a * b * np.sin(gamma) / V
    # v is the volume of the parallelepiped formed by the unit vectors a/|a|, b/|b|, b/|c|.
    v = V / (a * b * c)

    ALPHA = np.array([[1 / a, 0, 0], [-np.cos(gamma) / (a * np.sin(gamma)), 1 / (b * np.sin(gamma)), 0],
                      [(np.cos(gamma) * np.cos(alpha) - np.cos(beta)) / (a * v * np.sin(gamma)),
                       (np.cos(gamma) * np.cos(beta) - np.cos(alpha)) / (b * v * np.sin(gamma)),
                       np.sin(gamma) / (c * v)]])

    Dinv = np.array([[1 / astar, 0, 0], [0, 1 / bstar, 0], [0, 0, 1 / cstar]])
    Dinv = np.matrix(Dinv)
    fracADP = np.dot(Dinv, np.dot(np.transpose(ALPHA), np.dot(cartADP, np.dot(ALPHA, Dinv))))

    return fracADP


def ADPfrac2cart(cell, fracADP):
    (a, b, c, alphadeg, betadeg, gammadeg) = cell

    alpha = alphadeg / 360 * 2 * np.pi
    beta = betadeg / 360 * 2 * np.pi
    gamma = gammadeg / 360 * 2 * np.pi
    V = a * b * c * np.sqrt(1 - (np.cos(alpha)) ** 2 - (np.cos(beta)) ** 2 - (np.cos(gamma)) ** 2 \
                            + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    astar = b * c * np.sin(alpha) / V
    bstar = a * c * np.sin(beta) / V
    cstar = a * b * np.sin(gamma) / V
    # v is the volume of the parallelepiped formed by the unit vectors a/|a|, b/|b|, b/|c|.
    v = V / (a * b * c)

    BETA = np.array([
        [a, 0, 0],
        [b * np.cos(gamma), b * np.sin(gamma), 0],
        [c * np.cos(beta), c * (np.cos(alpha) - np.cos(beta) * np.cos(gamma)) / np.sin(gamma),
         c * v / np.sin(gamma)]]
    )

    D = np.array([[astar, 0, 0], [0, bstar, 0], [0, 0, cstar]])
    cartADP = np.dot(np.transpose(BETA), np.dot(D, np.dot(fracADP, np.dot(D, BETA))))
    return cartADP


def cart2pdb(number, name, cartesian):
    # reads number, name  and cartesian coordinates
    string = 'ATOM %6s %3s          0    %8.3f%8.3f%8.3f 1.000  0.00' \
             % (number, name, cartesian[0], cartesian[1], cartesian[2])
    return string


def cell2pdb(cell):
    # reads number, name  and cartesian coordinates
    string = 'CRYST1 %8.3f %8.3f %8.3f %6.2f %6.2f %6.2f' \
             % (cell[0], cell[1], cell[2], cell[3], cell[4], cell[5],)
    return string


##### Statistics function, returns rms mean and rmsd

def statistics(list):
    sum = 0.0000
    difsum = 0.00000

    if len(list) == 1 or len(list) == 0:
        return [0, 0]

    for i in range(len(list)):
        sum = sum + list[i] ** 2
    mean = np.sqrt(sum / len(list))

    for i in range(len(list)):
        difsum = difsum + (mean - list[i]) ** 2
        varians = difsum / (len(list) - 1)
        rmsd = np.sqrt(varians)
    return mean, rmsd


def Beta_ij_2_U_ij(beta_matrix, cell):
    """ Taking a matrix of beta_ij and transforming to Uijs.
    Needs the cell parameters."""

    (a, b, c, alphadeg, betadeg, gammadeg) = cell

    alpha = alphadeg / 360 * 2 * np.pi
    beta = betadeg / 360 * 2 * np.pi
    gamma = gammadeg / 360 * 2 * np.pi
    V = a * b * c * np.sqrt(1 - (np.cos(alpha)) ** 2 - (np.cos(beta)) ** 2 - (np.cos(gamma)) ** 2 \
                            + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    astar = b * c * np.sin(alpha) / V
    bstar = a * c * np.sin(beta) / V
    cstar = a * b * np.sin(gamma) / V

    beta_matrix = np.array(beta_matrix)

    U11 = beta_matrix[0][0] / (2 * np.pi ** 2 * astar * astar)
    U22 = beta_matrix[1][1] / (2 * np.pi ** 2 * bstar * bstar)
    U33 = beta_matrix[2][2] / (2 * np.pi ** 2 * cstar * cstar)
    U12 = beta_matrix[0][1] / (2 * np.pi ** 2 * astar * bstar)
    U13 = beta_matrix[0][2] / (2 * np.pi ** 2 * astar * cstar)
    U23 = beta_matrix[1][2] / (2 * np.pi ** 2 * bstar * cstar)
    U = np.array([[U11, U12, U13], [U12, U22, U23], [U13, U23, U33]])
    return U


def U_ij_2_Beta_ij(U_matrix, cell):
    """ Taking a matrix of U_ij and transforming to Beta_ijs
    Needs the cell parameters."""

    (a, b, c, alphadeg, betadeg, gammadeg) = cell

    alpha = alphadeg / 360 * 2 * np.pi
    beta = betadeg / 360 * 2 * np.pi
    gamma = gammadeg / 360 * 2 * np.pi
    V = a * b * c * np.sqrt(1 - (np.cos(alpha)) ** 2 - (np.cos(beta)) ** 2 - (np.cos(gamma)) ** 2 \
                            + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    astar = b * c * np.sin(alpha) / V
    bstar = a * c * np.sin(beta) / V
    cstar = a * b * np.sin(gamma) / V

    beta11 = U_matrix[0][0] * (2 * np.pi ** 2 * astar * astar)
    beta22 = U_matrix[1][1] * (2 * np.pi ** 2 * bstar * bstar)
    beta33 = U_matrix[2][2] * (2 * np.pi ** 2 * cstar * cstar)
    beta12 = U_matrix[0][1] * (2 * np.pi ** 2 * astar * bstar)
    beta13 = U_matrix[0][2] * (2 * np.pi ** 2 * astar * cstar)
    beta23 = U_matrix[1][2] * (2 * np.pi ** 2 * bstar * cstar)
    Beta = np.array([[beta11, beta12, beta13], [beta12, beta22, beta23], [beta13, beta23, beta33]])
    return Beta
