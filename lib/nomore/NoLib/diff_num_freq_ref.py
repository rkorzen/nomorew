#!/usr/bin/env python
# import re
# import sys
# from string import atof
# from math import exp
# from scipy import dot,transpose,array, eye, linalg
# from numpy import *
# from .Crystallographic import ADPcart2frac
import subprocess
from os import rename
#
# from .ADPindex import overlap
# from scipy import dot,transpose,array, eye, linalg, optimize
# from scipy.optimize import fmin, fmin_bfgs, fmin_cg, brute, anneal, fmin_l_bfgs_b, fmin_ncg, fmin_powell
# from numpy import *
# from .reader import readed_f
# from rot3D import rot3D

file = open('test.cif', 'w')
file.write("tresc")
file.close()
file = open('numb_ref_freq.data', 'w')
file.write("tresc")
file.close()
file = open('scaling_parameters.data', 'w')
file.write("tresc")
file.close()
file = open('frequencies.data', 'w')
file.write("tresc")
file.close()
outputNoMoRe=open('NoMoRe.out','w')
result = []
for i in range(10,40,10):
	print(i)
	p = subprocess.Popen(['./NoMoRe.py', 'Xylitol.out'], stdout=subprocess.PIPE)
	out, err = p.communicate()
#	outputNoMoRe.write('Number of refined frequencies: %i'%i)
	result.append(out)
#	outputshelx=open('test.cif',r)
	rename('test.cif', 'test_%i.cif'%i)
	rename('numb_ref_freq.data', 'numb_ref_freq.data_%i'%i)
	rename('scaling_parameters.data', 'scaling_parameters.data_%i'%i)
	rename('frequencies.data', 'frequencies.data_%i'%i)
	source=open('NoMoRe.py').readlines()
	aim=open('NoMoRe.py','w')
	for s in source:
		aim.write(s.replace("REFINE = '0-%i'"%i,"REFINE = '0-%i'"%(i+10)))
	aim.close()
for i in result:
	outputNoMoRe.write(i)
	
