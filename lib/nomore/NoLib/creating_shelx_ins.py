import re
from .msd_calculator import msd_calc
import subprocess
from .read_fcf import ReadFCF
import numpy as np

shelxl_path = "./shelxl"

"""
Python module to read a shelx.ins file and create a new shelx.ins with ADPs from msd_calculator.

"""


def create_shelx(
        degeneracy,
        refine, T, sg, normalmodes_orig,
        frequencies, atoms, symmatomcoord,
        cell, eliminate,
        ShelxInputFile,
        files
):
    msdmatrix, ADPs, ADPsch, ADPst = msd_calc(degeneracy, refine, T, sg, normalmodes_orig, frequencies, atoms,
                                              symmatomcoord, cell, eliminate, files)

    (logfile, debugfile, datafile) = files

    inputfile1 = ShelxInputFile  # read input file as argument.

    # input1 = open(inputfile1, 'r')
    with open(inputfile1, 'r') as input1:
        data = input1.readlines()

    atoms = []

    s1 = 0
    s2 = 0
    a1 = re.compile('FVAR')
    a2 = re.compile('HKLF')
    for i in range(len(data)):
        am1 = a1.search(data[i])
        am2 = a2.search(data[i])
        if am1:
            s1 = i
        if am2:
            s2 = i

    atoms = []
    for i in range(s1 + 1, s2):
        line = data[i].split()
        atom = line
        atoms.append(atom)

    newatoms = []
    joined = []
    for i in range(len(atoms)):
        for k in atoms[i]:
            if k == '=':
                joined = atoms[i] + atoms[i + 1]

                newatoms.append(joined)

        if len(atoms[i]) > 4 and len(atoms[i]) < 8:
            newatoms.append(atoms[i])
    orgADPs = []
    diff = []
    for i in range(len(newatoms)):
        a = newatoms[i]
        if not 'H' in a[0]:
            u11 = float(a[6])
            u22 = float(a[7])
            u33 = float(a[9])
            u23 = float(a[10])
            u13 = float(a[11])
            u12 = float(a[12])
            atomadp = u11, u22, u33, u23, u13, u12
            orgADPs.append(atomadp)

    for i in range(len(orgADPs)):
        TEMP = ADPst[i].split()
        TEMP2 = orgADPs[i]
        for j in range(6):
            a = TEMP2[j]
            b = float(TEMP[j])
            diff.append((a - b) ** 2)

    shelxl_header = data[0:s1 + 1]

    with open('test.ins', 'w') as shelxl_input:

        for line in shelxl_header:
            shelxl_input.write(line)
        shelxl_atoms_list = []
        for i in range(len(newatoms)):
            string = ''
            a = newatoms[i]
            a[2] = '%f' % (float(a[2]) + 10.)
            a[3] = '%f' % (float(a[3]) + 10.)
            a[4] = '%f' % (float(a[4]) + 10.)
            for k in a[0:6]:
                string = string + k + '  '

            string = string + ADPsch[i] + '\n'
            shelxl_atoms_list.append(string)
        for line in shelxl_atoms_list:
            shelxl_input.write(line)
        shelxl_input.write('HKLF 4')

    p = subprocess.Popen([shelxl_path, 'test'], stdout=subprocess.PIPE)
    p.wait()

    if p.returncode == 0:
        Fc2, Fm2, F2sig = ReadFCF('test.fcf', 'test')
        w_deltaF2 = []
        sum1 = 0
        sum2 = 0
        for i in range(len(Fc2)):
            a = 0.0
            P = (2 * float(Fc2[i]) + float(Fm2[i])) / 3.
            w = 1 / (float(F2sig[i]) + (a * P) ** 2)
            w_deltaF2.append(w * (float(Fc2[i]) - float(Fm2[i])))
            sum1 = sum1 + w ** 2 * (float(Fm2[i]) - float(Fc2[i])) ** 2
            sum2 = sum2 + w ** 2 * float(Fm2[i]) ** 2
        my_wR2 = np.sqrt(sum1 / sum2)

    if p.returncode == 0:
        wR2 = 0.9
        shelxloutput = open('test.lst', 'r')
        textf = shelxloutput.readlines()
        shelxloutput.close()
        p1 = re.compile('wR2')
        k = 0
        for i in range(len(textf)):
            liste = textf[i]
            m1 = p1.search(liste)
            if m1:
                if k == 0:
                    wR2 = float(liste[6:12])
                    k = + 1
                    np.set_printoptions(precision=2)
                    debugfile.write('%17.15f %17.15f\n' % (wR2, my_wR2))
                    debugfile.writelines(["%s " % item for item in np.array(sg[:11])])
                    debugfile.write('\n')

    else:
        # print('something is rotten. arghh')
        raise Exception('something is rotten. arghh')
        # exit()
    # print 'w_deltaF ',w_deltaF2[:10]
    # print 'my_wR2 ', my_wR2
    return w_deltaF2, my_wR2, sum1


def final_shelx_coordinates(files):
    (logfile, debugfile, datafile) = files

    with open("test.ins") as f:
        source = f.readlines()

    aim = open('test.ins', 'w')
    for s in source:
        aim.write(s.replace("L.S. 0", "L.S. 10"))
    aim.close()

    with open("test.ins") as f:
        source = f.readlines()

    aim = open('test.ins', 'w')
    for z in source:
        aim.write(z.replace("REM ACTA", "ACTA"))
    aim.close()

    p = subprocess.Popen([shelxl_path, 'test'], stdout=subprocess.PIPE)
    p.wait()

    with open('test.lst', 'r') as shelxloutput:
        textf = shelxloutput.readlines()

    p1 = re.compile('wR2')
    once = 0
    for i in range(len(textf)):
        liste = textf[i]
        m1 = p1.search(liste)
        if m1 and once == 0:
            wR2 = float(liste[6:13])
            logfile.write(f'wR2 {wR2} \n')
            once = 1
    # print('RunTLS' , RunTLS)
    return wR2
