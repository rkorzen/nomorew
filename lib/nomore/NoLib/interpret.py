from numpy import copy

# function that takes the parameters and expands the array
# to take the degeneracy of modes and the list of refined
# modes into account. returns the sg array, which contains
# scaling factors for all frequencies. 

def expand(parameters,degeneracy,refine,atoms):
    number_of_frequencies = atoms*3
    sg = [1.0] * number_of_frequencies
    ig = copy(sg)
    c = 0
    for i in refine:
        ig[i] = parameters[c]
        c = c+1
    for j in range(len(sg)):
        sg[j] = ig[degeneracy[j]]
    return sg
   

# function that translates the user-specified list
# of frequencies which should be refined into a list
# which python understands
# i.e. '1-5,8,10' -> [1,2,3,4,5,8,10]

def interpret(REFINE):
    if REFINE == 'None':
        return []
    b = REFINE.split(',')
    ref_list = []
    for i in b:
        j = i.split('-')
        if len(j) == 2:
            if int(j[0]) > int(j[1]):
                print('error: ', i, 'is not in the right format')
            c = list(range(int(j[0]),int(j[1])+1))
            for k in c:
                ref_list.append(k)
        else:
            ref_list.append(int(j[0]))
    return ref_list


# take the list of frequencies and search for symmetry.
# return a list giving for each freq the sequential 
# number of the first mode in the degeneracy.

def find_symmetry(frequencies):
    degenerate = [0]
    counter = 0
    tolerance = 0.001
    for f in range(1,len(frequencies)):
        if abs(frequencies[f] - frequencies[f-1]) < tolerance:
            degenerate.append(counter)
        else: 
            counter = f
            degenerate.append(counter)
    return degenerate


