#!/Applications/anaconda/bin/python


import CifFile as cf
def ReadFCF(filename,blockname):
    
    a = cf.CifFile(filename)
    H = a[blockname]['_refln_index_h']
    K = a[blockname]['_refln_index_k']
    L = a[blockname]['_refln_index_l']
    
    


    Fc2 = a[blockname]['_refln_F_squared_calc']
    Fm2 = a[blockname]['_refln_F_squared_meas']
    F2sig = a[blockname]['_refln_F_squared_sigma']
    return Fc2,Fm2,F2sig

#ReadFCF('test.fcf','test')






