#!/usr/bin/env python
import sys
from scipy import dot,transpose,array, eye, linalg
from numpy import *
import numpy as np
from scipy import dot,transpose,array, eye, linalg, optimize
import pickle
import scipy

sp = open('Hessian.pickled', 'rb')
Hessian = pickle.load(sp)
sp.close()

print('Hessian')
print(Hessian)

print('eigenvalues')
print(linalg.eig(Hessian))


Hhinv = linalg.inv(Hessian)
print(Hhinv)


print('Standard uncertainties - ')
print('Square root of diagonal of inverse Hessian')
for i in range(len(Hhinv)):
    print(Hhinv[i][i])
    print(np.sqrt(Hhinv[i][i]))




