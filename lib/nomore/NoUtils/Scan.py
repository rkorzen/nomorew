#!/usr/bin/env python

import sys

sys.path.insert(0,'/Users/anders/src/NoMoRe/NoLib') 


import re
import sys
from string import atof
from math import exp
from scipy import dot,transpose,array, eye, linalg
from numpy import *
import numpy as np
from Crystallographic import ADPcart2frac

from ADPindex import overlap
from scipy import dot,transpose,array, eye, linalg, optimize
from scipy.optimize import fmin, fmin_bfgs, fmin_cg, brute
from reader import readed_f
from msd_calculator import msd_calc
from creating_shelx_ins import create_shelx, final_shelx_coordinates
from interpret import interpret, find_symmetry, expand
import subprocess
import pickle



"""
Scan the error function along one parameter.
"""


T=298
eliminate=''
REFINE=''
Points = 100.
Range = 0.1
parameter_number = 0

def Scan(PickledResultFile,CrystalOutputFile,ShelxInputFile,ShelxHKLFile, T=T,eliminate=eliminate,REFINE=REFINE,Points=Points,Range=Range,parameter_number=parameter_number):
    
    file = open(PickledResultFile, 'rb')

    result = pickle.load(file)
    file.close()


    refine = interpret(REFINE)

    normalmodes_orig, cell, freqnum, frequencies, atoms, symmatomcoord  = readed_f(CrystalOutputFile)


    parameters = [1.0]*len(refine)
    degeneracy = find_symmetry(frequencies)


    def shelxl(parameters):
        sg = expand(parameters,degeneracy,refine,atoms)
        return create_shelx(degeneracy, refine, T, sg, normalmodes_orig, frequencies,atoms,symmatomcoord,cell, eliminate,ShelxInputFile)[2]


    def shelxl_vec(parameters):
        sg = expand(parameters,degeneracy,refine,atoms)
        return np.array(create_shelx(degeneracy, refine, T, sg, normalmodes_orig, frequencies,atoms,symmatomcoord,cell, eliminate,ShelxInputFile)[0])


    
    parameters = result['x']

    print('parameters')
    print(parameters)
    print('parameter number')
    print(parameter_number)
    print('parameter value')
    print(parameters[parameter_number])
    X = parameters[parameter_number]
    Y = shelxl(parameters)


    param = []
    errorfunction = []
    step = Range/Points
    stepvector = [0.0]*len(refine) 
    parameters[parameter_number] = parameters[parameter_number] - step*Points/2
    for i in range(Points):
        stepvector[parameter_number] += step
        #print 'stepvector ', stepvector
        newparameters = parameters + stepvector
        errorfunction.append(shelxl(newparameters))
        param.append(newparameters[parameter_number])
    return param, errorfunction, X, Y


