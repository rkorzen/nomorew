#! /usr/local/bin/python
import re
import sys
from string import atof
from math import exp
from scipy import dot,transpose,array, eye, linalg
from numpy import *
import os

sys.path.insert(0, os.path.abspath('..'))
import NoLib
from NoLib.reader import readed_f
from NoLib.msd_calculator import msd_calc
from NoLib.interpret import interpret, find_symmetry, expand
import NoUtils
from NoUtils.Covariances import Covariances
from NoUtils.Scan import Scan
from NoLib.Crystallographic import ADPcart2frac
"""

What modes are significant
for normal mode refinement?

Depends on the type of atom,
the frequency and the temperature.


copyleft,
Anders O. Madsen
version: May 2018


"""



#### USER INPUT ####
T=122                        # The temperature.
eliminate = -3               # The number of frequencies to eliminate.
anharm_corr = 174.           # correction of anharmonicity for X-H stretch vibrations.
gruneisen = 1.0              # starting point for gruneisen parameter (scaling otherwise non-refined frequencies).
TEMPERATURES = [122]
inputfile = 'Lala.out'
#### END USER INPUT ####


msds_freq_T = []

normalmodes_orig, cell, freqnum, frequencies_orig, atoms, symmatomcoord  = readed_f(inputfile)
FREQUENCIES = atoms*3
refine = list(range(FREQUENCIES))
degeneracy = find_symmetry(frequencies_orig)
        
msds_freq = []
# looping over the frequencies
for f in range(FREQUENCIES):
	sg = [1000]*(atoms*3+1) 
	sg[f+1] = 1.00
	msdmatrix, ADPs, ADPsch, ADPst = msd_calc(degeneracy,refine,T, sg, normalmodes_orig, frequencies_orig,atoms,symmatomcoord,cell, eliminate)
	msds_freq.append(msdmatrix)

# looping over atoms:
counter = 0 

import matplotlib.pyplot as plt


print('debug')
print(msds_freq)

allplot = []
atom_types =[]
# treating one atom at a time
for a in range(0,freqnum,3):
        msd_sum = 0
	if symmatomcoord[counter][5] == 'T':
	        atom_type = symmatomcoord[counter][9:10]
		print('atom type ', atom_type)
		plot = []
                # looping over all frequencies
		for i in range(len(msds_freq)):
			A = msds_freq[i][a:a+3,a:a+3]
			B = ADPcart2frac(cell,A)
			E =  linalg.eig(A)
			Ueq = sqrt(E[0][0]**2 + E[0][1]**2 + E[0][2]**2)
			Ba = array(B)
                        Ueq_w = Ueq * float(atom_type) # Ueq weighed by the atomic number
			plot.append(Ueq_w)
		allplot.append(plot)
		atom_types.append(atom_type)
	counter = counter + 1

summed = [0]*(len(allplot[0]))
for i in range(len(allplot)):
        for k in range(len(allplot[0])):
                summed[k] += allplot[i][k]
        
print(summed)

total_sum = 0
percent_sum = []
for s in summed:
        total_sum += s
for s in summed:
        percent_sum.append(s/total_sum)



        
legends = []

colours = {'1':'black','7':'blue','6':'green','8':'red'}
alphas = {'1':0.2,'7':0.5,'6':0.5,'8':0.5}
print('atom_types ', atom_types)

plt.plot(percent_sum)

#for i in range(len(allplot)):
#	plt.plot(allplot[i],'o',color=colours[atom_types[i]],alpha=alphas[atom_types[i]])
#	legends.append(str(i))
#plt.legend(legends)
plt.show()





		


