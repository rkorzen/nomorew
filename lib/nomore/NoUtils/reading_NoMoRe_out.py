import re
import sys
from string import atof
from math import exp
from scipy import dot,transpose,array, eye, linalg
from numpy import *
from Crystallographic import ADPcart2frac




inputfile=sys.argv[1]        # read input file as argument.
input = open(inputfile,'r')

data = input.readlines()

a1 = re.compile('wR2')
a2 = re.compile('normal mode')
a3 = re.compile('RunTLS')
for i in range(len(data)):
	    am1 = a1.search(data[i])
	    am2 = a2.search(data[i])
	    am3 = a3.search(data[i])
	    if am1:
		s=i
		print(data[s][0:15])
	    if am2:
		r=i
		print(data[r][0:70])	
	    if am3:
		p=i
		print('RunTLS:', data[p+1][0:30])	
