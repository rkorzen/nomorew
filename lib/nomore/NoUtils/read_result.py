#!/usr/bin/env python

import numpy as np
from numpy import linalg
import pickle
import sys




try: 
    pickled_output_file = sys.argv[1]
    print('pickled_output_file', pickled_output_file)
    file = open(pickled_output_file, 'rb')
except:
    file = open('least_squares_result.data', 'rb')


"""
try:
    pickled_shelxl_files = sys.argv[2]
    print 'pickled shelxl files', pickled_shelxl_files
    file2 = open(pickled_shelxl_files,'rb')
except: 
    pass

"""
result = pickle.load(file)
file.close()

#result2 = pickle.load(file2)
#file2.close()


#for line in result2[0]:
#    sys.stdout.write(line)


print('Status of refinement')
print(result['message'])

np.set_printoptions(linewidth=150)
np.set_printoptions(precision=2)

print('final values')
print(result['x'])


print('Gradient')
print(result['grad'])

jac = result['jac']


Hess = np.dot(np.transpose(jac),jac)

np.set_printoptions(precision=1)
np.set_printoptions(suppress=True)

print('Hessian')
print(np.array(Hess))
np.set_printoptions(suppress=False)

np.set_printoptions(precision=2)
print('Inverse Hessian')
print(np.array(linalg.inv(Hess)))


np.set_printoptions(suppress=True)

print('Square root of the diagonal elements of inverse Hessian')
print('- Estimated standard uncertainties.')

Hess_inv = np.array(linalg.inv(Hess))

for i in range(len(result['x'])):
    print('%6.4f'%np.sqrt(Hess_inv[i][i]))



dimension = len(Hess_inv[0])

print('Covariances')

length= len(Hess)
CovarianceMatrix = np.zeros((length,length))
for i in range(dimension):
    for j in range(dimension):
        CovarianceMatrix[i][j] =  Hess_inv[j][i] / np.sqrt(Hess_inv[j][j]) / np.sqrt(Hess_inv[i][i])

print(CovarianceMatrix)


print('final values for new start')
np.set_printoptions(precision=10)

print(result['x'])

exit()


## Another way to get the Hessian - numdifftools :

import numdifftools as nd
from interpret import interpret, find_symmetry, expand
from creating_shelx_ins import create_shelx, final_shelx_coordinates
from reader import readed_f

#### USER INPUT ####
T=123                        # The temperature.
eliminate = -3               # The number of frequencies to eliminate.
anharm_corr = 174.           # correction of anharmonicity for X-H stretch vibrations.
REFINE = '0-10' # modes to refine
gruneisen = -1.0              # starting point for gruneisen parameter (scaling otherwise non-refined frequencies).
#### END USER INPUT ####

normalmodes_orig, cell, freqnum, frequencies, atoms, symmatomcoord  = readed_f()
refine = interpret(REFINE)
degeneracy = find_symmetry(frequencies)
parameters = result['x']



def shelxl(parameters):
	sg = expand(parameters,degeneracy,refine,atoms)
	return create_shelx(degeneracy, refine, T, sg, normalmodes_orig, frequencies,atoms,symmatomcoord,cell, eliminate)[2]

G = nd.Gradient(shelxl,step=nd.MinStepGenerator(base_step=0.1))
H =  nd.Hessian(shelxl,full_output=True,step=nd.MinStepGenerator(base_step=0.1))



#print 'Numerical estimate of the Gradient'
#Gg = G(parameters)
#print Gg
print('--------------')
print('--------------')
print('Numerical estimate of Hessian')
Hh = H(parameters)
print(Hh)
Hhm = Hh[0]
print('Eigenvalues of Hessian')
eigHhm = linalg.eig(Hhm)
print(eigHhm)


print('--------------')
print('--------------')
print('Inverse Hessian')
Hhinv = linalg.inv(Hhm)
print(Hhinv)


