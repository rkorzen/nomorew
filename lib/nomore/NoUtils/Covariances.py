#!/usr/bin/env python

import sys

sys.path.insert(0,'../NoLib') 


import re
import sys
#from string import atof
from math import exp
from scipy import dot,transpose,array, eye, linalg
from numpy import *
import numpy as np
from Crystallographic import ADPcart2frac

from ADPindex import overlap
from scipy import dot,transpose,array, eye, linalg, optimize
from scipy.optimize import fmin, fmin_bfgs, fmin_cg, brute
from reader import readed_f
from msd_calculator import msd_calc
from creating_shelx_ins import create_shelx, final_shelx_coordinates
from interpret import interpret, find_symmetry, expand
import subprocess
import pickle



"""
Covariance estimation of NoMoRe refinement.
"""


T=298
eliminate=''
REFINE=''


def Covariances(PickledResultFile,CrystalOutputFile,ShelxInputFile,ShelxHKLFile, T=T,eliminate=eliminate,REFINE=REFINE):



    file = open(PickledResultFile, 'rb')

    result = pickle.load(file)
    file.close()


    refine = interpret(REFINE)

    normalmodes_orig, cell, freqnum, frequencies, atoms, symmatomcoord  = readed_f(CrystalOutputFile)


    print('final values')
    print(result['x'])

    print('Gradient')
    print(result['grad'])

    jac = result['jac']


    parameters = [1.0]*len(refine)
    degeneracy = find_symmetry(frequencies)


    def shelxl(parameters):
            sg = expand(parameters,degeneracy,refine,atoms)
            return create_shelx(degeneracy, refine, T, sg, normalmodes_orig, frequencies,atoms,symmatomcoord,cell, eliminate,ShelxInputFile)[1]


    def shelxl_vec(parameters):
            sg = expand(parameters,degeneracy,refine,atoms)
            return np.array(create_shelx(degeneracy, refine, T, sg, normalmodes_orig, frequencies,atoms,symmatomcoord,cell, eliminate,ShelxInputFile)[0])


    
    parameters = result['x']

    print('parameters')
    print(parameters)


    import numdifftools as nd
    D = nd.Derivative(shelxl,method='central')
    G = nd.Gradient(shelxl,full_output=True,step=nd.core.MinStepGenerator(base_step=0.0001))
    H = nd.Hessian(shelxl,full_output=True,step=nd.core.MinStepGenerator(base_step=0.007))
    J = nd.Jacobian(shelxl_vec,full_output=True,step=nd.core.MinStepGenerator(base_step=0.007))


    print('Final parameters')
    print(parameters)


    # R-factor wR(F^2) is 0.097643 at optimized freqs. 
    # wR(F^2) = sqrt( sum |w|Fo^2 - Fc^2|| / sum|wFo^2|)

    # the least squares residual is sum |w|Fo^2 - Fc^2||, so 
    # I should square the wR(F^2) and multiply by the denominator sum|wFo^2|
    # in order to get the residual. This residual should be used as the maximum
    # likelihood estimator, and be used in the computation of errors via the 
    # inverse Hessian matrix.


    

    




    np.set_printoptions(precision=10)
    


    print('Numerical estimate of the Jacobian')
    Jj = J(parameters)
    print(Jj)


    print('Numerical estimate of the Gradient')
    Gg = G(parameters)
    print(Gg)
    print('--------------')
    print('--------------')

    exit()

    print('Numerical estimate of Hessian')
    Hh = H(parameters)
    print(Hh)
    Hhm = Hh[0]
    print('--------------')
    print('--------------')
    print('Inverse Hessian')
    Hhinv = linalg.inv(Hhm)
    print(Hhinv)



    # pickle the Hessian
    print('pickling the Hessian in the file Hessian.pickled')
    f = open('Hessian.pickled', 'wb')
    pickle.dump(Hhm,f)
    f.close()




