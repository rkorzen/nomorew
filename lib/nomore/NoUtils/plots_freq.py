#!/usr/bin/env python
import re
import sys
from string import atof
from math import exp
from scipy import dot,transpose,array, eye, linalg
from numpy import *
from Crystallographic import ADPcart2frac
import pickle
from reader import readed_f

from ADPindex import overlap
from scipy import dot,transpose,array, eye, linalg, optimize
from scipy.optimize import fmin, fmin_bfgs, fmin_cg, brute, anneal, fmin_l_bfgs_b, fmin_ncg, fmin_powell
from numpy import *
from msd_calculator import msd_calc
import matplotlib.pyplot as pyplot
import numpy as np

normalmodes_orig, cell, freqnum, frequencies, atoms, symmatomcoord  = readed_f()

for i in range(10,30,10):
	refine=[]
	ref = open('numb_ref_freq.data_%i'%i, 'rb')
	refine = pickle.load(ref)
	print(refine)

	freq_opt=[]
	sp = open('scaling_parameters.data_%i'%i, 'rb')
	freq_opt = pickle.load(sp)

	freq_opt2 = freq_opt[1:]

	print(freq_opt2)

	freq_f=[]
	f = open('frequencies.data_%i'%i, 'rb')
	freq_f = pickle.load(f)
	freq_ff = freq_f[1:]

#	print freq_ff
#	pyplot.plot(refine,freq_opt2)
#	pyplot.savefig('scaling_fajny_wykres_%i.png'%i)

	pyplot.plot(refine,freq_ff,label='%i ref freq'%i)
	pyplot.title('REFINED FREQUENCIES')
	pyplot.xlabel('Number of refined frequencies')
	pyplot.ylabel('Freq/cm-1', rotation='vertical')
#	pyplot.savefig('fajny_wykres2_%i.png'%i)

a=21
reffreq=frequencies[:a]
reffreq.pop(0)
reffreq.pop(0)
reffreq.pop(0)
reffreq.insert(0, 50.0)
reffreq.insert(0, 50.0)
reffreq.insert(0, 50.0)
pyplot.plot(refine,reffreq,linewidth=2.0, color='black', label='CRYSTAL09')
pyplot.legend(loc='lower right')
pyplot.savefig('xyl_631g.png')


