from django.contrib import admin

# Register your models here.
from refinements.models import Refinement, Result


@admin.register(Refinement)
class RefinementAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'user', 'created']


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    list_display = ['id', 'refinement', 'status']

