from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
from core import storages
from nomorew.settings import RESULT_CHOICES


class Refinement(models.Model):
    name = models.CharField(
        max_length=200,
        null=True, blank=True,
        help_text="Name - for identification purposes"
    )
    description = models.TextField(
        null=True, blank=True,
        help_text="Description - only avaiable in this application"
    )
    crystal_output_file = models.FileField(
        verbose_name=_("Crystal output file"),
        # storage=storages.get_storage('refinements'),
        upload_to="cof/%Y%m%d/%H%M",
        help_text=_("Crystal Frequency output")
    )
    shelx_input_file = models.FileField(
        verbose_name=_("Shelx input file"),
        # storage=storages.get_storage('refinements'),
        upload_to="shelxinput/%Y%m%d/%H%M",
        help_text=_("Template input to shelxl")

    )
    shelx_hkl_file = models.FileField(
        verbose_name=_("Shelx HKL file"),
        # storage=storages.get_storage('refinements'),
        upload_to="shelxhkl/%Y%m%d/%H%M",
        help_text=_("")
    )
    T = models.IntegerField(
        verbose_name=_("The temperature"),
        default=122
    )
    eliminate = models.IntegerField(
        verbose_name=_("Eliminate"),
        default=-3,
        help_text=_("The number of frequencies to eliminate")
    )
    anharm_corr = models.FloatField(default=174.0)
    refine = models.CharField(
        verbose_name=_("Refine"),
        max_length=20,
        default="0-17",
        help_text=_("Modes to refine"),
    )
    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"<Refinement {self.name} | {self.user}>"


class Result(models.Model):
    refinement = models.ForeignKey(Refinement, on_delete=models.CASCADE, related_name="results")
    status = models.CharField(max_length=20, choices=RESULT_CHOICES)
    zip_file = models.FileField(
        verbose_name=_("Zipped results"),
        storage=storages.get_storage("results"),
        upload_to="%Y%m%d/%H%M",
        blank=True,
        null=True,
    )
    error_msg = models.TextField(blank=True, null=True)
