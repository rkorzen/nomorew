from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.urls import reverse

from refinements.forms import RefinementForm
from refinements.models import Refinement


def refinements_list(request):

    if request.POST:
        form = RefinementForm(request.POST, request.FILES)

        if form.is_valid():
            refinement = form.save(commit=False)
            refinement.user = request.user
            refinement.save()
            messages.add_message(request, messages.SUCCESS, "Refinement was added")
            return redirect(reverse("dashboard"))
    else:
        form  = RefinementForm()

    return render(
        request,
        "refinements/refinements_list.html",
        {'form': form}
    )


def refinements_details(request, id):
    refinement = get_object_or_404(Refinement, pk=id)

    return render(
        request,
        "refinements/refinement_details.html",
        {'refinement': refinement}
    )

def dashboard(request):
    refinements = Refinement.objects.filter(user=request.user)

    return render(
        request,
        "refinements/dashboard.html",
        {'refinements': refinements}
    )