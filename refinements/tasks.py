import os
import traceback
import zipfile

from celery import shared_task
from django.conf import settings
from django.core.files import File
from django.core.mail import send_mail
from django.template.loader import render_to_string

from lib.nomore import NoMoRe
from lib.nomore.NoLib.write_cif import write_final_cif
from refinements.models import Refinement

import logging

def send_finish_email(result):
    try:
        if result.status == "success":
            message = render_to_string('emails/success.html', {
                'result': result,
                'domain': settings.DEFAULT_DOMAIN,

            })

            send_mail(
                subject="NoMoRe Calculations ended with success",
                message=message,
                from_email=settings.EMAIL,
                recipient_list=[result.refinement.user.email],
            )

        else:
            message = render_to_string('emails/failure.html', {
                'result': result,
                'domain': settings.DEFAULT_DOMAIN,

            })

            send_mail(
                subject="NoMoRe Calculations ended with failure",
                message=message,
                from_email=settings.EMAIL,
                recipient_list=[result.refinement.user.email],
            )
        logging.info(f"Send email to: {result.refinement.user.email}")
    except Exception as e:
        logging.error("Failed send email to: {result.refinement.user.email}", exc_info=True)


def call_nomore(instance):
    """First step - refine calculates"""

    zf = NoMoRe.refine(
        instance.crystal_output_file.path,
        instance.shelx_input_file.path,
        instance.shelx_hkl_file.path,
        T=instance.T,
        eliminate=instance.eliminate,
        REFINE=instance.refine
    )
    return zf


def zip_files(zip_file_name):
    write_final_cif()

    # prepare file list
    files = os.listdir()
    files_to_zip = files.copy()
    files_to_zip.remove('shelxl')
    # for f in files_to_zip:
    #     if f.startswith("fort"):
    #         files_to_zip.remove(f)

    # add files to zip
    with zipfile.ZipFile(zip_file_name, 'w') as zf:
        for f in files_to_zip:
            zf.write(f)

    # remove files
    for f in files:
        os.remove(f)

    return os.path.realpath(zip_file_name)


@shared_task
def make_refinement(refinement_id):
    refinement = Refinement.objects.get(pk=refinement_id)
    result = refinement.results.last()

    try:

        zf = call_nomore(refinement)

        if zf:
            zip_files(zf)
            result.status = "success"
            zip_file = File(open(zf, "rb"))
            result.zip_file.save(zf, zip_file)
            os.remove(zf)
            if os.path.basename(os.getcwd()).startswith("calc"):
                os.rmdir(os.getcwd())

        else:
            result.status = "failed"


    except Exception as e:
        result.status = "failed"
        result.error_msg = traceback.format_exc()
    result.save()
    os.chdir(settings.BASE_DIR)

    send_finish_email(result)


@shared_task
def adding_task(x, y):
    return x + y
