from django import forms

from refinements.models import Refinement


class RefinementForm(forms.ModelForm):

    class Meta:
        model = Refinement
        exclude = ['user']