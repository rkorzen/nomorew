from django.apps import AppConfig


class RefinementsConfig(AppConfig):
    name = 'refinements'

    def ready(self):
        import refinements.signals