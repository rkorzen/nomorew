
from django.db.models.signals import post_save
from django.dispatch import receiver

from refinements.models import Refinement, Result
from refinements.tasks import make_refinement

@receiver(post_save, sender=Refinement)
def create_result_for_refinement(sender, instance, **kwargs):
    Result.objects.create(refinement=instance, status="pending")
    make_refinement.delay(instance.id)

