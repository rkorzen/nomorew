from django.urls import path

from refinements.views import refinements_list, refinements_details, dashboard

urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('add/', refinements_list, name='refinements'),
    path('<int:id>', refinements_details, name="refinement")
]
