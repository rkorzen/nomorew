from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import CustomUser


# Register your models here.

class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ('email', 'is_staff', 'is_active', 'email_confirmed')
    list_filter = ('email', 'is_staff', 'is_active')

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ("RODO", {"fields": ('rodo_agree', )}),
        ('Permissions', {'fields': ('is_staff', 'is_active')})
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')
        })
    )

    search_fields = ('email',)
    ordering = ('email',)

    def email_confirmed(self, obj):
        return obj.profile.email_confirmed


admin.site.register(CustomUser, CustomUserAdmin)
