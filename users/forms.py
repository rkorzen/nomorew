from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from .models import CustomUser



class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    rodo_agree = forms.NullBooleanField(label="""I hereby give consent for my personal data to be processed for the purposes of licensing. I am aware that
providing personal data is voluntary, but necessary for the purposes for which it was collected, as well
as that I have the right to access my data and correct it.""")

    def clean_rodo_agree(self):
        rodo_agree = self.cleaned_data['rodo_agree']
        if rodo_agree is not True:
            raise ValidationError("this consent is necessary")
        return rodo_agree

    class Meta:
        model = CustomUser
        fields = ('email', 'password1', 'password2', 'rodo_agree')
